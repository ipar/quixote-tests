import os
import sys
import glob
import numpy as np
import pytest

#Quixote modules
from quixote import SolpsData
from quixote.extensions.chords import Chord
from quixote.tools import module_path, yaml_loader, SimpleNamespace, read_config
from quixote.tools import equal
from quixote.tools import get
from quixote.tools import update






@pytest.fixture(scope="session",
        params=[
            #'wg_d3d_sas_138299_ipar_sha_no_ammonx',
            'wg_d3d_sas_138299',
            '308_d3d_sas_138299',
            '94908', #For now?
            '119603',
            ])
def rundir(request):
    path = os.path.join('rundir_src',request.param,'run')
    return SolpsData(path)


@pytest.fixture(scope="session",
        params=[
            '94908', #For now?
            #'119603',
            ])
def rundir_structured(request):
    path = os.path.join('rundir_src',request.param,'run')
    return SolpsData(path)

@pytest.fixture(scope="session",
        params=[
            'wg_d3d_sas_138299',
            ])
def rundir_unstructured(request):
    path = os.path.join('rundir_src',request.param,'run')
    return SolpsData(path)

@pytest.fixture(scope="session",
        params=[
            'wg_d3d_sas_138299',
            '308_d3d_sas_138299',
            ])
def rundir_new(request):
    path = os.path.join('rundir_src',request.param,'run')
    return SolpsData(path)




## Chords need a mother, so probably is better to set some "real *.coords"
## in the SolpsData cases and try it from there.
#@pytest.fixture(scope="session",
#        params=[
#            ##Free
#            'XPT-1',
#            ])
#def chords(request):
#    loses = {}
#    # To avoid problems, no overlapping LOS, or this will be a toss up
#    files = glob.glob("losfiles/*.coords")
#    for path in files:
#        loses = update(loses, tools.parse_los_file(path))
#    
#    return Chord(los, loses[los][0], loses[los][1])






#--------------------- TOOLS ----------------------------------------------

def runs_description():
    return yaml_loader(os.path.join('runs_description.yaml')) #pytest only works in tests folder.






##--------------------- Validation tools -----------------------------------

def validity_rundir(rundir, var, var2=None):
    case_name = os.path.basename(os.path.dirname(rundir.path))
    npy_path = os.path.join(rundir.path, '../run_npy')

    try:
        if var2 is None:
            var2 = var
        runvar = get(rundir,var)
        npyvar = np.load(os.path.join(npy_path, var2+'.npy'))

    except:
        pytest.skip('npy not found!')
        return False

    return equal(runvar, npyvar)



### For each chord, test the chord itself, or the validity given a
###
### Why not put this in the test directly???



#--------------------- Skipping   tools ---------------------------------------
def skip_by_b2_species(sim, ns):
    if sim.ns < ns:
        pytest.skip('Number of fluid species is just {}.'.format(str(sim.ns)))

def skip_by_eirene_species(sim, neirene):
    if sim.natm < neirene[0]:
        pytest.skip('natm is just {0} instead of {1}.'
        .format( str(sim.natm), neirene[0]))
    if sim.nmol < neirene[1]:
        pytest.skip('nmol is just {0} instead of {1}.'
        .format(str(sim.nmol), neirene[1]))
    if sim.nion < neirene[2]:
        pytest.skip('nion is just {0} instead of {1}.'
        .format(str(sim.nion), neirene[2]))

def skip_b2standalone(sim):
    if sim.b2standalone:
        pytest.skip('Test not for B2standalone simulation.')

def skip_unstructured(sim):
    if sim.grid_type == 'unstructured':
        pytest.skip('Test not for Unstructured simulations.')
def skip_classical(sim):
    if sim.classical:
        pytest.skip('Test not for Classical simulations.')

def skip_structured(sim):
    if sim.grid_type == 'structured':
        pytest.skip('Test not for Structured simulations.')

def skip_dn(sim):
    if sim.magnetic_geometry.lower() in ['cdn', 'ddn', 'ddnu']:
        pytest.skip('Test not for DN simulations.')

def skip_sn(sim):
    if sim.magnetic_geometry.lower() in ['lsn', 'usn']:
        pytest.skip('Test not for SN simulations.')






