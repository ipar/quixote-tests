#In the future: separate into the different tools:
# test_tools_utils
# test_tools_core
# etc. to keep better track of what is missing.
# And also the importance of the test failure
# e.g. failure in util or core vs plots.

import os
import sys
import numpy as np
import pytest

import quixote.tools as tools

##ATTENTION: Missing test! (Not exhaustive list)
#def test_priority():
#def test_missing(case):
#def test_whichclass():
#def test_parse_dataframe():
#def test_elements():
#def test_ionization_potentials():


##ATTENTION: ADD NAN CASES!!
## Also, use np.testing.assert_equal and assert_array_equal
@pytest.mark.parametrize("case", [
    'None',
    'self negatives', 
    'cross negatives',
    'nan',
    'arrays',
    'dict', 
    'list',
    'tuple',
    'dict_nan',
    'list_nan',
    'tuple_nan',
    ])
def test_equal(case):

    a = np.array([[1,2.004], [3,4]])
    b = np.array([[1,2], [3,4]])

    c = np.array([[1,2.004], [np.nan,4]])
    d = np.array([[1,2], [np.nan,4]])

    structures={
    'dict':  [{'this':a, 'that': 22}, {'this':b, 'that': 22.0}],
    'list':  [[a, 22], [b, 22.0]],
    'tuple': [(a, 22), (b, 22.0)]}

    structures_nan={
    'dict_nan':  [{'this':c, 'that': 22}, {'this':d, 'that': 22.0}],
    'list_nan':  [[c, 22], [d, 22.0]],
    'tuple_nan': [(c, 22), (d, 22.0)]}

    if case == 'None':
        assert tools.equal(None, None)

    elif case == 'self negatives':
        assert tools.equal(0, 0)
        assert tools.equal({}, {})
        assert tools.equal([], [])
        assert tools.equal((), ())
        assert tools.equal(False, False)

    elif case == 'cross negatives':
        assert not tools.equal('', 0)
        assert not tools.equal('', {})
        assert not tools.equal('', [])
        assert not tools.equal('', ())
        assert not tools.equal('', False)
        assert not tools.equal('', None)

        assert not tools.equal({}, 0)
        assert not tools.equal({}, [])
        assert not tools.equal({}, ())
        assert not tools.equal({}, False)
        assert not tools.equal({}, None)

    elif case == 'nan':
        #assert tools.equal(np.nan, np.nan) ## This doesn't work yet.
        assert not tools.equal(a, d)
        assert not tools.equal(a, d, rtol=0.01)
        assert not tools.equal(c, d)
        assert tools.equal(c, d, rtol=0.01)

    elif case == 'arrays':
        assert not tools.equal(a, b)
        assert tools.equal(a, b, rtol=0.01)

    elif case in ['dict', 'list', 'tuple']:
        e1, e2 = structures[case]
        assert not tools.equal(e1, e2)
        assert not tools.equal(e1,e2, rtol=0.01, strict=True)
        assert tools.equal(e1,e2, rtol=0.01)

    elif case in ['dict_nan', 'list_nan', 'tuple_nan']:
        e1, e2 = structures_nan[case]
        assert not tools.equal(e1, e2)
        assert not tools.equal(e1,e2, rtol=0.01, strict=True)
        assert tools.equal(e1,e2, rtol=0.01)




@pytest.mark.parametrize("case", ['creation', 'attachment'])
@pytest.mark.parametrize("space", ['SimpleNamespace','snv'])
def test_namespaces(space, case):
    a = {'testing': 2, 'test':4.002}
    if space == 'SimpleNamespace':
        tmp = tools.SimpleNamespace(**a)
    elif space == 'snv':
        tmp = tools.snv(**a)

    if case == 'creation':
        assert tmp.testing == 2
        assert tmp.test == 4.002
    elif case == 'attachment':
        tmp.new = 'test'
        assert tmp.new == 'test'



##ATTENTION: get or test with elipsis, etc.
@pytest.mark.parametrize("case", ['shallow','deep','deeper'])
def test_get(case):
    tmp = tools.SimpleNamespace(test=4.002)
    tmp2 = tools.SimpleNamespace(testing=2)
    tmp3 = tools.SimpleNamespace(deep='glup')
    tmp2.not_shallow = tmp3
    tmp.new = tmp2
    if case == 'shallow':
        assert tools.get(tmp, 'test') == 4.002
    elif case == 'deep':
        assert tools.get(tmp,'new.testing') == 2
    elif case == 'deeper':
        assert tools.get(tmp,'new.not_shallow.deep') == 'glup'




@pytest.mark.parametrize("case", ['self','empty','valid', 'replacement'])
def test_update(case):
    a = {'testing wrong': 2, 'test':4.002}
    b = {'wrong testing': 2, 'test':4}
    c = {'different': 'yeah','test':'four'}
    if case == 'self':
        assert tools.update(a) == a
    elif case == 'empty':
        assert tools.update({},a) == a
        assert tools.update(a,{}) == a
    elif case == 'valid':
        assert tools.update(a,b) == {
            'testing wrong': 2, 'test': 4, 'wrong testing': 2}
        assert tools.update(b,a) == {
            'wrong testing': 2, 'test': 4.002, 'testing wrong': 2}
        assert tools.update(a,c) == {
            'testing wrong': 2, 'test': 'four', 'different': 'yeah'}
        assert tools.update(c,a) == {
            'different': 'yeah', 'test': 4.002, 'testing wrong': 2}
    elif case == 'replacement':
        tmp = tools.update(a,b)
        assert tmp == {'testing wrong': 2, 'test': 4, 'wrong testing': 2}
        a['testing wrong'] = 'NO!'
        assert tmp == {'testing wrong': 2, 'test': 4, 'wrong testing': 2}
        a['testing wrong'] = 2
        tmp['testing wrong'] = 'NO!'
        assert tools.update(a,b) == {
                'testing wrong': 2, 'test': 4, 'wrong testing': 2}




@pytest.mark.parametrize("case", ['default', 'self','empty','single', 'multiple'])
def test_purge(case):
    a = {'testing wrong': 2, 'test':4.002, 'different': 'yeah', 'exp': None}
    b = {'testing wrong': 2, 'test':4.002, 'different': 'yeah'}
    if case == 'default':
        assert tools.purge(a) == {
            'testing wrong': 2, 'test':4.002, 'different': 'yeah'}
    elif case == 'self':
        assert tools.purge(b) == b
    elif case == 'empty':
        assert tools.purge({},a) == {}
        assert tools.purge(a,{}) == a
    elif case == 'single':
        assert tools.purge(a, None) == {
            'testing wrong': 2, 'test':4.002, 'different': 'yeah'}
        assert tools.purge(a, 'yeah') == {
            'testing wrong': 2, 'test':4.002, 'exp': None}
        assert tools.purge(a, 4.002) == {
            'testing wrong': 2, 'different': 'yeah', 'exp': None}
        assert tools.purge(a, 2) == {
            'test':4.002, 'different': 'yeah', 'exp': None}
    elif case == 'multiple':
        assert tools.purge(a, [None, 'yeah']) == {'testing wrong': 2, 'test':4.002}
        assert tools.purge(a, ['yeah', None]) == {'testing wrong': 2, 'test':4.002}
        assert tools.purge(a, [None, 4.002]) == {
            'testing wrong': 2, 'different': 'yeah'}
        assert tools.purge(a, [4.002, 2]) == {'different': 'yeah', 'exp': None}




@pytest.mark.parametrize("case",
        ['empty', 'None', 'nulls', 'single', 'multiple'])
def test_extend(case):
    a = {'testing wrong': 2, 'test':4.002, 'different': 'yeah', 'exp': None}
    b = -999999
    c = np.arange(3)
    d = [4,7,9]
    e = [2.9]
    f = ['alfalfa', 100]

    if case == 'empty':
        assert tools.extend([]) == []
    elif case == 'None':
        assert tools.extend(None) == []
    elif case == 'nulls':
        assert tools.extend(0) == [0]
    elif case == 'single':
        assert tools.extend(a) == [a]
        assert tools.extend(b) == [b]
        assert tools.extend(c) == list(c)
        assert tools.extend(d) == d
        assert tools.extend(f) == f
    elif case == 'multiple':
        assert tools.equal(tools.extend(a,c), [a] + list(c))
        assert tools.extend(b,c) == [b] + list(c)
        assert tools.extend(e,b,c) == e + [b] + list(c)
        assert tools.extend(e,b,f) == e + [b] + f






@pytest.mark.parametrize("case",
    ['self','empty','negatives','matches', 'arrays','replacement'])
def test_intersection(case):
    a = {'testing wrong': 2, 'test':4.002}
    b = {'wrong testing': 2, 'test':4}
    c = {'different': 'yeah','test':'four'}
    d =  {'array': np.array([[1,2],[3,4]]), 'test':'four'}
    d2 = {'array': np.array([[2,2],[3,4]]), 'test':'four'}
    d3 = {'array': np.array([[1,2],[3,4]]), 'test':5}
    if case == 'self':
        assert tools.intersection(a) == a

    elif case == 'empty':
        assert tools.intersection({},a) == {}
        assert tools.intersection(a,{}) == {}

    elif case == 'negatives':
        assert tools.intersection(a,b) == {}
        assert tools.intersection(a,c) == {}
        assert tools.intersection(b,c) == {}

    elif case == 'matches':
        assert tools.intersection(a,b,rtol=0.01) == {'test':4.002}
        assert tools.intersection(b,a,rtol=0.01) == {'test':4}

    elif case == 'arrays':
        assert tools.intersection(c,d) == {'test':'four'}
        assert tools.intersection(d,d2) == {'test':'four'}

        tmp = {'array': np.array([[1,2],[3,4]])}
        for k, v in tools.intersection(d,d3).items():
            assert np.allclose(tmp[k], v)

    elif case == 'replacement':
        tmp = tools.intersection(a,b,rtol=0.01) 
        assert tmp == {'test':4.002}
        a['test'] = 8.00
        assert tmp == {'test':4.002}
        a['test'] = 4.002
        tmp['test'] = 19
        assert tools.intersection(a,b,rtol=0.01) == {'test':4.002}




@pytest.mark.parametrize("case", ['None', 'no dict', 'empty', 'shallow',
    'deep', 'deeper'])
def test_nested_depth(case):
    a = {'shallow': True}
    aa = {'deep':{'brain': 'developed'}, 'ambivalent': True}
    aaa = {'deep':{'brain': 'developed'}, 'ambivalent': True, 'sense':aa}
    if case == 'basics':
        assert tools.nested_depth(None) == 0
        assert tools.nested_depth(2.0) == 0
    elif case == 'no dict':
        assert tools.nested_depth([2.0, 'two']) == 0
        assert tools.nested_depth((2.0, 2)) == 0
    elif case == 'empty':
        assert tools.nested_depth({}) == 1
    elif case == 'shallow':
        assert tools.nested_depth(a) == 1
    elif case == 'deep':
        assert tools.nested_depth(aa) == 2
    elif case == 'deeper':
        assert tools.nested_depth(aaa) == 3




@pytest.mark.parametrize("case",
    ['str', 'float', 'int', 'list', 'dict', 'tuple', 'ndarray'])
def test_null(case):
    if case == 'str':
        assert tools.null('asdf') == ''
    elif case == 'dict':
        assert tools.null({'a':10, 'b':[10]}) == {}
    elif case == 'list':
        assert tools.null([10, 'a']) == []
    elif case == 'float':
        assert tools.null(15.2) == 0.0
    elif case == 'int':
        assert tools.null(7) == 0
    elif case == 'tuple':
        assert tools.null((111, '4')) == ()
    elif case == 'ndarray':
        assert np.allclose(
        tools.null(np.array([[10, 15], [22,  34]])), np.zeros((2,2)))








@pytest.mark.parametrize("braces", [True, False])
@pytest.mark.parametrize("case", ['None', 'empty', 'simple', 'simplified'])
def test_latex_unit(case, braces):
    if braces:
        xstr = ['[',']']
    else:
        xstr = ['', '']
    a = '10^19 m^-3 eV'
    a_result = xstr[0]+'$10^{19}\\,\\mathrm{m}^{-3}\\,\\mathrm{eV}$'+xstr[1] 

    b = '10^19 m^3 eV s^-1 m^-2 * photons*MW eV^-1'
    b_result = xstr[0]+'$10^{19}\\,\\mathrm{m}\\,\\mathrm{s}^{-1}\\,\\mathrm{photons}\\,\\mathrm{MW}$'+xstr[1]


    if case == 'None':
        assert tools.latex_unit(None, braces=braces) == ''
    elif case == 'emtpy':
        assert tools.latex_unit('[]', braces=braces) == ''
        assert tools.latex_unit('', braces=braces) == ''
        assert tools.latex_unit([], braces=braces) == ''

    elif case == 'simple':
        assert tools.latex_unit(a, braces=braces) == a_result
    
    elif case == 'simplified':
        assert tools.latex_unit(b, braces=braces) == b_result





@pytest.fixture(scope="session", params=['default', 'default+local'])
def config(request):
    #pytest.set_trace()
    if request.param == 'default':
        return tools.read_config(include_local=False)
    elif request.param == 'default+local':
        # For now, in the future monkeypatch with a
        # set "local" here to test deep update.
        return tools.read_config(include_local=False)

@pytest.mark.parametrize("case",
    ['Files', 'Family', 'Chords', 'HeatFlux', 'Matplotlib style', 'Plots' ])
def test_read_config(config, case):
    assert case in config
    ## Copy and paste the default parameters and just keep updating
    ## as the parameters are updated on the code.





## Experimental
def test_f90nml():
    """ It works, but it doesn't like things like
    userfluxparm(1,1) = bunch of numbers.
    It wants userfluxparm(:,1) = bunch of numbers"""


    from quixote.tools.experimental import parser_f90namelist
    nml = parser_f90namelist('mockup_namelists/idx.nml')

    assert nml['idx_nml']['v'] == [1,2e3,3,4,5]





