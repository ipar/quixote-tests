import os
import sys
import numpy as np
import pytest
import json

import quixote.tools as tools


from mockups import rundir_unstructured
from mockups import rundir_new
from generate_data_fixtures import chord_var_names


@pytest.mark.parametrize("line, p1, p2, expected", [
    (tools.Line([0.0, 0.0], [1.0, 0.0], ltype='points'), 
    np.array([0.5, 0.5]),
    np.array([5.0, -0.5]),
    False),

    (tools.Line([0.0, 0.0], [1.0, 0.0], ltype='points'), 
    np.array([0.5, -0.5]),
    np.array([5.0, -0.5]),
    True),

    (tools.Line([0.0, 0.0], [1.0, 0.0], ltype='points'), 
    np.array([0.5, -0.5]),
    np.array([0.5, -0.5]),
    True),

    (tools.Line([0.0, 0.0], [1.0, 1.0], ltype='points'), 
    np.array([-1.0, 1.0]),
    np.array([1.0, 0.0]),
    False),

    ])
def test_points_same_side_line(p1, p2, line, expected):
    assert tools.points_on_same_side_of_line(p1,p2,line) == expected



#@pytest.mark.parametrize("p1, p2, expected", [
#    ])
#def test_distance(p1, p2, expected):



@pytest.mark.parametrize("line1, line2, p", [
    (tools.Line([0,1], [1,0]), tools.Line([0,0], [1,1]), np.array([0.5, 0.5])),
    (tools.Line([1,1], [2,2]), tools.Line([0,0], [1,1]), None),
    ])
def test_find_intersection(line1, line2, p):
    assert tools.equal(tools.find_intersection(line1, line2), p)



@pytest.mark.parametrize("icell, p, expected", [
    ('cell1', np.array([1.28, -0.18]), True),
    ('cell1', np.array([1.295, -0.18]), False),

    ('cell2', np.array([0.425, 1.196]), True),
    ('cell2', np.array([0.425, 1.190]), False),
    ('cell2', np.array([0.425, 1.200]), False),
    ])
def test_point_in_cell(icell, p, expected):
    cell = {
    'cell1':np.array([
    [1.25833061, -0.2382142 ],
    [1.26989703, -0.24154287],
    [1.2979672 , -0.12012274],
    [1.28639144, -0.11828589]]),
    'cell2':np.array([
    [0.43507278, 1.19150338],
    [0.40929998, 1.20553244],
    [0.41023215, 1.20301732],
    [0.43591086, 1.1888437 ]]),
    }
    assert tools.equal(tools.point_in_cell(cell[icell], p), expected)





@pytest.mark.parametrize('style', ['free', 'diaggeom'])
def test_parse_los_files(style):
    """
    How to create the file:
    loses = tools.parse_los_file('free.coords')
    with open('free.json', 'w') as f:
         dumped = json.dumps(loses, cls=NumpyEncoder)
     or: dumped = tools.json_numpy(loses)
         json.dump(dumped, f)
    """
    fcoords = tools.parse_los_file(os.path.join('losfiles', style+'.coords'))
    with open(os.path.join('losfiles', style+'.json'), 'r') as f:
        fjson = json.loads(json.load(f))
    assert tools.equal(fcoords, fjson)





def test_names(rundir_new):
    chords1 = list(rundir_new.chords.chords.keys())
    chords2 = list(np.load(os.path.join(
        rundir_new.rundir,'../run_npy/chords_keys.npy')))
    assert tools.equal(chords1, chords2)


@pytest.mark.parametrize('method',
    ['start', 'end', 'los', 'bins', 'length', 'cdlength', 'indices', 'lsc'])
@pytest.mark.parametrize('los',
    ['INN-1','OUT-1', 'OUT-2', 'MID-1', 'MID-2', 'XPT-1'])
def test_characteristics(rundir_new, los, method):
    case_name = os.path.basename(os.path.dirname(rundir_new.path))
    npy_path = os.path.abspath(os.path.join(rundir_new.path, '../run_npy/chords'))
    chord = rundir_new.chords[los]
    runvar = tools.get(chord, method)
    npyvar = np.load(os.path.join(npy_path,
        '{}_{}.npy'.format(method, los)))
    assert tools.equal(runvar, npyvar)




@pytest.mark.parametrize('method',
    ['evaluate', 'integrate', 'maximum', 'minimum', 'binned', 'average'])
@pytest.mark.parametrize('var', chord_var_names)
@pytest.mark.parametrize('los',
    ['INN-1','OUT-1', 'OUT-2', 'MID-1', 'MID-2', 'XPT-1'])
def test_functions(rundir_new, los, var, method):
    case_name = os.path.basename(os.path.dirname(rundir_new.path))
    npy_path = os.path.abspath(os.path.join(rundir_new.path, '../run_npy/chords'))
    chord = rundir_new.chords[los]
    runvar = tools.get(chord, method)(var)
    npyvar = np.load(os.path.join(npy_path,
        '{}_{}_{}.npy'.format(method, los, var)))
    assert tools.equal(runvar, npyvar)
