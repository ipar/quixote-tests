"""
"""
import os
#from IPython import embed
import numpy as np

from quixote import SolpsData
import quixote.tools as tools
from quixote.tools import postmortem


common_geometry_names = [
    'grid', 'vessel',
    'crx', 'cry',
    ]

common_snapshot_names = [
    'zn', 'am', 'am_kg', 'mi', 'zeff',
    'te', 'ti', 'ne', 'na', 'naeir', 'ua', 'po',
    'na_isonuclear', 'fractional_abundance',
    'cimp', 'ni',
    'pstot', 'ptot', 'psi', 'pse', 'pdyna',
    'fna', 'fnax', 'fnay',
    'fhe', 'fhex', 'fhey',
    'fhi', 'fhix', 'fhiy',
    'fchp',
    'kinrgy', 'kinetic_energy',
    'stored_energy',

    ]
#st_snapshot_names = common_snapshot_names + [
#    
#    ]

common_eirene_names = [
    'dab2', 'dmb2', 'dib2',
    'tab2', 'tmb2', 'tib2',
    ]


common_sources_names = [
    'sna', 'particle_source', 'resco'
    ]



chord_var_names = [
    'te', 'ti', 'ne', 'na', 'naeir',
    'pstot', 'ptot', 'psi', 'pse', 'pdyna',
    'dab2', 'dmb2', 'tab2', 'tmb2', 'particle_source'
    ]



tt_common_names = [
    'time',
    #'ds',
    ]

@postmortem
def main_func():

    # -------------------------------------------
    # TEST CASES:

    # -------------------------------------------
    #data = UnstructuredData('rundir_src/wg_d3d_sas_138299_ipar_sha_no_ammonx/run/')
    #gcommon = True
    #gchords = True

    # -------------------------------------------
    #data = SolpsData('rundir_src/wg_d3d_sas_138299/run')
    #gcommon = False # Done
    #geval = False # Done
    ## Currently doesn't have all the ds
    #gtt = True

    # -------------------------------------------
    data = SolpsData('rundir_src/308_d3d_sas_138299/run')
    gcommon = False # Done
    geval = False # Done
    gtt = True

    # -------------------------------------------

    if not os.path.exists('tmp_npy'):
        os.makedirs('tmp_npy')
    path = lambda name: os.path.join('tmp_npy',name)

    print("Creating data from {}".format(
        os.path.basename(data.rundir_rel('..'))))


    if gcommon:
        for var in common_geometry_names:
            np.save(path(var), getattr(data, var))
            print("Saved: {}".format(var))

        for var in common_snapshot_names:
            np.save(path(var), getattr(data, var))
            print("Saved: {}".format(var))

        for var in common_eirene_names:
            np.save(path(var), getattr(data, var))
            print("Saved: {}".format(var))

        for var in common_sources_names:
            np.save(path(var), getattr(data, var))
            print("Saved: {}".format(var))



    if geval:
        if not os.path.exists(path('chords')):
            os.makedirs(path('chords'))
        pathc = lambda name: os.path.join(path('chords'), name)


        for chord in data.chords.values():
            for method in ['start', 'end', 'los', 'bins', 'length',
                'cdlength', 'indices', 'lsc']:
                namevar = "{}_{}".format(method, chord.name)
                runvar = tools.get(chord, method)
                np.save(pathc(namevar), runvar)
                print("Saved: {}".format(namevar))

            for method in ['evaluate', 'integrate', 'maximum',
                'minimum', 'binned', 'average']:
                for var in chord_var_names:
                    namevar = "{}_{}_{}".format(method, chord.name, var)
                    runvar = tools.get(chord, method)(var)
                    np.save(pathc(namevar), runvar)
                    print("Saved: {}".format(namevar))




    if gtt:
        if data.magnetic_geometry in ['cdn', 'ddn', 'ddnu']:
            locs = ['out','inn','imp','omp', 'out2', 'inn2']
        else:
            locs = ['out','inn','imp','omp']

        for loc in locs:
            tt = getattr(data,loc)
            for var in tt_common_names:
                np.save(path('{}.{}'.format(loc,var)), getattr(tt, var))
                print("Saved: {}.{}".format(loc,var))













    return


if __name__ == '__main__':
    main_func()
