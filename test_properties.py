# Idea for test: timing test
# Creation of SolpsData should remain below certain threshold.
# time before calling, time after, and assert that the lapse is below X s.


import os
import sys
import numpy as np
import pytest
from quixote import module_path, read_config
from quixote.tools import create_log, modify_log

from mockups import rundir, validity_rundir
from mockups import rundir_structured
from mockups import skip_by_b2_species, skip_by_eirene_species, skip_b2standalone
#from mockups import skip_ddn, skip_sn, skip_no_mdsplus_module

from generate_data_fixtures import common_geometry_names
from generate_data_fixtures import common_snapshot_names
from generate_data_fixtures import common_eirene_names
from generate_data_fixtures import common_sources_names


alog = create_log('quixote', level='debug')


@pytest.mark.parametrize('var',
    common_geometry_names + 
    common_snapshot_names +
    common_eirene_names +
    common_sources_names)
def test_rundir(rundir, var):
    if var in common_eirene_names:
        skip_b2standalone(rundir)
    assert validity_rundir(rundir, var)



def test_iiomp_iimp(rundir):
    assert np.min(np.abs(rundir.bb[...,2][rundir.iimp])) > np.max(np.abs(rundir.bb[...,2][rundir.iomp]))




if __name__ == '__main__':
    import sys, pytest
    pytest.main(sys.argv)




#------------------------------------------------------------------------------


#ident_names = ['solpsversion', 'solps_code', 'exp']
#
#geometry_names = ['vessel_mesh', 'grid', 'nx', 'ny','ns', 'natm','nmol',
#    'nnreg', 'region', 'regflx', 'regfly', 'regvol', 'cr', 'cr_x', 'cr_y',
#    'cz', 'cz_x', 'cz_y','r', 'z',
#    'imp','omp','sep',
#    #'ixp', 'oxp', #INCONSISTENT DEFINITION ACCROSS CLASSES FOR NOW.
#    'za','zn',
#    'am', 'b','pitch','pitch_pt', 'pitch_tot','invpitch', 'dv', 'hx', 'hy',
#    'parallel_surface_m', 'parallel_surface', 'ipardspar', 'domp', 'leftix',
#    'leftiy', 'rightix', 'rightiy', 'bottomix', 'bottomiy', 'topix', 'topiy',
#    'nncut', 'rightcut', 'leftcut', 'topcut', 'bottomcut',
#    #'xpoint', #ATTENTION: It doesn't seem to work for DDNU
#    'sy', 'sx', 'domp_parallel', 'mi', 'am_kg',
#    'vessel', 'gs',
#    ]
#
#snapshot_names = ['te', 'ti', 'ne', 'na', 'zeff', 'ua', 'po', 'pstot','pdynd',
#    'fmox', 'fmoy', 'fna', 'fnax', 'fnay', 'fna32', 'fnax32', 'fnay32',
#    'fna52', 'fnax52', 'fnay52', 'na_eirene', 'na_atom','fractional_abundances',
#    'fhe', 'fhex', 'fhey','fhi', 'fhix', 'fhiy','fhj', 'fhjx', 'fhjy',
#    'fhm', 'fhmx', 'fhmy',
#    'fhp', 'fhpx', 'fhpy',
#    #'fht', 'fhtx', 'fhty',
#    'fch', 'fchx', 'fchy',
#    'stored_energy', 'ni',
#    #'pressure_gradient_force', #ASK FHITZ
#    'cimp', 'pdyna','ptot',
#    'dperp', 'kyiperp', 'kyeperp',
#    'rpt', 'rqahe', 'rqrad', 'line_radiation',
#    'rrahi', 'rramo', 'rrana', 'rsahi', 'rsamo', 'electric_field',
#    #'electric_force' #ASK FHITZ
#    ]
#
#snapshot_eirene_names = ['tab2', 'dab2', 'tmb2', 'dmb2', 'tib2','dib2' ]
#
#
#residuals_names = ['smo','smq','smav']
#
#
#
