set title noenhanced
set key noenhanced
set nolog x
set nolog y
set format y "%g"
set zero 1e-50
set timestamp
set xlabel "time"
set ylabel ""
set title ".../puff=4.0e21_pump=0.001_drifts_divchiconst"
plot \
"gnuplot.data" using 1:2 title "fnixap1" with lines, \
"gnuplot.data" using 1:3 title "fnixap2" with lines
pause 3600
